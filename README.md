Assessment:

Use PHP and MySQL code to build 2 docker images.

Team members:
Baur
Maheen

1) What we worked on 

Baur:
- Container 1: MySQL Database Server
- Container 2: Apache Web Server with PHP - i.e. an application (app)
- Debugging

Maheen:
- docker-compose.yaml
- Debugging

2) How to use it
- Open GitBash
- git clone https://Gambit706@bitbucket.org/jamb95/devops-assessment.git
- Run linux simulator (e.g. MobaXTerm)
- Run command "docker-compose up"
- Open URL *yourmachinename*.conygre.com:80
